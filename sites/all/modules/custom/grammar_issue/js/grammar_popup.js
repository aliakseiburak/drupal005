(function($, document) {
    Drupal.behaviors.grammarIssue = {
        attach: function (context, settings) {
            var selectLength = settings.grammarIssue.selectLength;
            var popupUrl = settings.grammarIssue.popupUrl;

            $(document).keyup(function (e) {
                if ((e.which == 10 && e.modifiers == 2) ||
                    (e.keyCode == 0 && e.charCode == 106 && e.ctrlKey) ||
                    (e.keyCode == 13 && e.ctrlKey)) {
                    new GrammarIssueDialog(selectLength, popupUrl);
                }
            });
        }
    };

    var GrammarIssueDialog = function(length, popupUrl) {
        this.length = length;
        this.popupUrl = popupUrl;

        this.init();
    };

    GrammarIssueDialog.prototype = {
        constructor: GrammarIssueDialog,
        //@todo prevent dialog creation when dialog created
        init: function() {
            if (!this.validateBrowser()) {
                return;
            }
            try {
                var selObj = this.getSelectedObject();

                if (null == selObj) {
                    return;
                }
                if (!this.validateSelectedTextLength(selObj.toString())) {
                    return;
                }
                var context = this.extractContext(selObj);
            } catch (e) {
                return;
            }
            var normalContext = this.normalizeContext(context);
            this.callDialog(normalContext);
        },
        validateBrowser: function() {
            if (navigator.appName.indexOf("Netscape") != -1 && eval(navigator.appVersion.substring(0, 1)) < 5) {
                this.displayAlert();
                return false;
            }
            return true;
        },
        displayAlert: function() {
            alert('You browser do not support select text capture. Probably too old version or some other issue.');
        },
        getSelectedObject: function() {
            var selObj = null;
            if (document.getSelection) {
                var selObj = document.getSelection();
            } else {
                // Implement IE <10 compatibility.
                var selObj = document.selection;
            }
            return selObj;
        },
        validateSelectedTextLength: function(text) {
            return text.length <= this.length;
        },
        extractContext: function(selObj) {
            var pre = '',
                text = null,
                suf = '',
                pos = -1;

            if (selObj.getRangeAt) {
                var r = selObj.getRangeAt(0);
                text = r.toString();
                var textBefore = document.createRange();
                textBefore.setStartBefore(r.startContainer.ownerDocument.body);
                textBefore.setEnd(r.startContainer, r.startOffset);
                pre = textBefore.toString();
                var textAfter = r.cloneRange();
                textAfter.setStart(r.endContainer, r.endOffset);
                textAfter.setEndAfter(r.endContainer.ownerDocument.body);
                suf = textAfter.toString();
            } else {
                if (selObj.createRange) {
                    var r = selObj.createRange();
                    text = r.text;
                    var textBefore = selObj.createRange();
                    textBefore.moveStart("character", -60);
                    textBefore.moveEnd("character", -text.length);
                    pre = textBefore.text;
                    var textAfter = selObj.createRange();
                    textAfter.moveEnd("character", 60);
                    textAfter.moveStart("character", text.length);
                    suf = textAfter.text;
                } else {
                    text = selObj.toString();
                }
            }
            var p;
            var s = (p = text.match(/^(\s*)/)) && p[0].length;
            var e = (p = text.match(/(\s*)$/)) && p[0].length;
            pre = pre + text.substring(0, s);
            suf = text.substring(text.length - e, text.length) + suf;
            text = text.substring(s, text.length - e);

            if ('' == text) {
                return;
            }
            return {
                pre: pre,
                text: text,
                suf: suf,
                pos: pos
            };
        },
        normalizeContext: function(context) {
            var pre = context.pre.substring(context.pre.length - 60, context.pre.length).replace(/^\S{1,10}\s+/, '');
            var suf = context.suf.substring(0, 60).replace(/\s+\S{1,10}$/, '');
            var normalContext = pre + '<!!!>' + context.text + '<!!!>' + suf;
            return normalContext.toString().replace(/[\r\n]+/g, ' ').replace(/^\s+|\s+$/g, '');
        },
        callDialog: function(normalContext) {
            var url = Drupal.settings.basePath + this.popupUrl;
            var link = $('<a></a>').attr('href', url).addClass('ctools-use-modal-processed').click(Drupal.CTools.Modal.clickAjaxLink);
            var nodeId = $('.node').attr('id').match(/\d+$/)[0];

            Drupal.ajax[url] = new Drupal.ajax(url, link.get(0), {
                url: url,
                event: 'click',
                progress: {type: 'throbber'},
                submit: {
                    'js': true,
                    'passage': normalContext,
                    'nodeId': nodeId
                }
            });
            link.click();
        },
    };

})(jQuery, document);
