<?php
/**
 * @file
 * Contains main Ctools popup form callbacks and form processing.
 */

/**
 * Popup’s callback function for form.
 */
function grammar_issue_popup_callback($js = NULL) {

  if (!$js) {
    // If JavaScript is disabled – we output the form without the popup
    return drupal_get_form('grammar_issue_popup_form');
  }

  ctools_include('ajax');
  ctools_include('modal');
  // Setting up the initial form preferences.
  $form_state = [
    'ajax' => TRUE,
    'title' => t('Spelling error in text'),
  ];

  $output = ctools_modal_form_wrapper('grammar_issue_popup_form', $form_state);
  print ajax_render($output);
  drupal_exit();
}

/**
 * Build form within a popup that contains selected text and additional field for user comment.
 */
function grammar_issue_popup_form($form, &$form_state) {

  $post = $_POST;

  if (!isset($post['passage']) || !isset($post['nodeId'])) {
    $form['grammar_issue'] = [
      '#title' => t('Error!'),
      '#markup' => t('Unexpected behaviour'),
    ];
    return $form;
  }

  $form['#prefix'] = '<div id="ajax-container"></div>';
  //@todo setup default text
  $form['passage'] = [
    '#title' => t('Some text'),
    '#markup' => '<p>' . $post['passage'] . '</p>',
  ];
  $form['comment'] = [
    '#title' => t('You may leave comment here'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  ];
  $form['node_nid'] = [
    '#type' => 'hidden',
    '#value' => $post['nodeId'],
  ];
  $form['issue'] = [
    '#type' => 'hidden',
    '#value' => $post['passage'],
  ];
  $form['button'] = [
    '#type' => 'button',
    '#value' => t('Send'),
    '#ajax' => [
      'callback' => 'grammar_issue_popup_form_process',
      'wrapper' => 'ajax-container',
    ]
  ];
  $form['cancel'] = [
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#ajax' => [
      'callback' => 'grammar_issue_popup_form_dismiss',
      'wrapper' => 'ajax-container',
    ]
  ];

  return $form;
}

/**
 * Processed form values and triggers e-mail being sent.
 * Forms appropriate return message in the popup.
 */
function grammar_issue_popup_form_process($form, &$form_state) {
  if (saveGrammarIssueEntity($form_state['values'])) {
    $command = createCtoolsCommand(
      'Confirm completion',
      '<h1>Thank you. Our content administrators will get an e-mail related to your request.</h1>'
    );
  } else {
    $command = createCtoolsCommand(
      'Failure',
      '<h1>Unable to process you request. Please try again later or inform site administrator if problem persists.</h1>'
    );
  }

  $email = variable_get('grammar_issue_sent_to_email', variable_get('site_mail', ''));
  drupal_mail('grammar_issue', 'notification', $email, language_default(), $form_state['values']);

  print ajax_render([$command]);
  exit;
}

/**
 * Populates modal with appropriate content.
 */
function createCtoolsCommand($title, $message) {
  return ctools_modal_command_display(t($title), t($message));
}

/**
 * Dismiss the modal.
 */
function grammar_issue_popup_form_dismiss($form, &$form_state) {
  print ajax_render([ctools_modal_command_dismiss()]);
  exit;
}
