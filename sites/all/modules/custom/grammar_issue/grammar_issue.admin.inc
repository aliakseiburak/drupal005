<?php
/*
 * @file
 */

/**
 * Returns the render array for the form.
 */
function grammar_issue_settings() {
  $form = [];

  $form['send_to_email'] = [
    '#title' => t('E-mail for sending grammar issues to'),
    '#description' => t('Enter e-mail. Leave blank to use site administrator e-mail.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('grammar_issue_sent_to_email', variable_get('site_mail', ini_get('sendmail_from'))),
    '#size' => 50,
    '#maxlength' => 50,
    '#required' => FALSE,
  ];

  $form['select_length'] = [
    '#title' => t('Max select text lenth'),
    '#description' => t(
      'Sets max length of text that user is able to send to review in a single popup.'
    ),
    '#type' => 'textfield',
    '#default_value' => variable_get('grammar_issue_select_length', 25),
    '#size' => 2,
    '#maxlength' => 2,
    '#required' => TRUE,
    '#element_validate' => ['element_validate_integer_positive'],
  ];

  $form['popup_text'] = [
    '#title' => t('Popup message'),
    '#description' => t('Message that user will see in popup.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('grammar_issue_popup_text', ''),
    '#rows' => 3,
    '#maxlength' => 128,
    '#required' => TRUE,
  ];

  $form['ip_limit'] = [
    '#title' => t('Day limit from one IP'),
    '#description' => t(
      'Sets a threshold of issues being sent from one IP address duaring the day.'
    ),
    '#type' => 'textfield',
    '#default_value' => variable_get('grammar_issue_ip_limit', 10),
    '#size' => 3,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#element_validate' => ['element_validate_integer_positive'],
  ];
  $form['#validate'][] = 'grammar_issue_settings_validate';
  $form['#submit'][] = 'grammar_issue_settings_submit';

  return system_settings_form($form);
}

/**
 * Validates the form.
 */
function grammar_issue_settings_validate($form, &$form_state) {
  $email = $form_state['values']['send_to_email'];

  if ('' != $email && !valid_email_address($email)) {
    form_set_error(
      'send_to_email',
      t('The email address appears to be invalid.')
    );
  }
}

/**
 * Adds a submit handler/function to the form to process user made
 * config changes.
 */
function grammar_issue_settings_submit($form, &$form_state) {
  $email = $form_state['values']['send_to_email'];
  $selectLength = $form_state['values']['select_length'];
  $popupText = $form_state['values']['popup_text'];
  $ipLimit = $form_state['values']['ip_limit'];

  variable_set('grammar_issue_sent_to_email', $email);
  variable_set('grammar_issue_select_length', $selectLength);
  variable_set('grammar_issue_popup_text', $popupText);
  variable_set('grammar_issue_ip_limit', $ipLimit);
}
