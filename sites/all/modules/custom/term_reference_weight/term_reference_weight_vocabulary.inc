<?php
/**
 * @file
 * Additional file for term references weights.
 */

/**
 * Builds form for a term edit page.
 *
 * @param array $form
 * @param array $form_state
 * @param object|array $edit
 *
 * @return mixed
 */
function term_reference_weight_vocabulary_edit_refs_weights($form, &$form_state, $edit = array()) {
  // During initial form build, adds the entity to the form state for use
  // during form building and processing. During a rebuild, uses what is in the
  // form state.
  if (!isset($form_state['vocabulary'])) {
    $vocabulary = is_object($edit) ? $edit : (object) $edit;
    $defaults = array(
      'name' => '',
      'machine_name' => '',
      'description' => '',
      'hierarchy' => 0,
      'weight' => 0,
    );
    foreach ($defaults as $key => $value) {
      if (!isset($vocabulary->$key)) {
        $vocabulary->$key = $value;
      }
    }
    $form_state['vocabulary'] = $vocabulary;
  }
  else {
    $vocabulary = $form_state['vocabulary'];
  }

  $form['intro'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="content"><p>' . t('This page allows you to manage term references weights of each entity type they attached to.') . '</p></div>',
  );

  $references = _term_reference_weight_get_vocabulary_terms_references($vocabulary);

  foreach ($references as $entity_type => $entity_type_reference) {
    $form_state['entity_types'][] = $entity_type;
    $form['#tree'] = TRUE;
    $form['term_references_weights'][$entity_type] = array(
      '#type' => 'fieldset',
      '#title' => t('%term term references weights', array('%term' => $entity_type)),
      '#tree' => TRUE,
      '#theme' => 'term_reference_weight_theme_references_weights_control',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    _term_reference_weight_form_element_fill_references_weights_control($form['term_references_weights'][$entity_type], $entity_type_reference);
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  if (!empty($references)) {
    $form_state['#trw_save'] = TRUE;
  }
  else {
    $form['term_references_weights']['#theme'] = 'term_reference_weight_theme_references_weights_control';
  }

  return $form;
}

/**
 * Form submit.
 *
 * Works on 'Save' operation only.
 *
 * @param array $form
 * @param array $form_state
 */
function term_reference_weight_vocabulary_edit_refs_weights_submit($form, &$form_state) {
  // Prevents term references weights save on delete submit.
  if (!isset($form_state['confirm_delete'])) {
    foreach ($form_state['entity_types'] as $entity_type) {
      foreach ($form_state['values']['term_references_weights'][$entity_type] as $value) {
        if (isset($value['general_hidden'])) {
          _term_reference_weight_set_reference_weight($value['general_hidden'], $value['weight']);
        }
      }
    }
    drupal_set_message(t('The references weights saved.'));
  }
}

/**
 * Builds form output for every entity.
 *
 * @param array $element
 * @param array $entity_type_reference
 */
function _term_reference_weight_form_element_fill_references_weights_control(&$element, $entity_type_reference) {
  foreach ($entity_type_reference as $key => $reference) {
    $element[$key] = array();
    $element[$key]['general_hidden'] = array(
      '#type' => 'hidden',
      '#value' => array(
        $reference['field_instance']['field_name'],
        $reference['term']['tid'],
        $reference['bundle_name'],
        $reference['entity']['type'],
        $reference['entity']['id'],
      ),
    );
    $element[$key]['entity_name'] = array(
      '#type' => 'markup',
      '#markup' => sprintf('%s <i>(%s)</i>', l($reference['entity']['label'], check_plain($reference['entity']['path'])), $reference['entity']['label']),
    );
    $element[$key]['field_human_title'] = array(
      '#type' => 'markup',
      '#markup' => sprintf('<span title="%s">%s</span>', $reference['field_instance']['field_name'], $reference['field_instance']['label']),
    );
    $element[$key]['term_name'] = array(
      '#type' => 'markup',
      '#markup' => (isset($reference['term']['name']) ? l($reference['term']['name'], 'taxonomy/term/' . $reference['term']['tid'] . '/edit') : t('<missing term>')),
    );
    $element[$key]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#title_display' => 'invisible',
      '#delta' => 10000,
      '#default_value' => $reference['weight'],
    );
  }
}

/**
 * Retrieves all references for terms from given vocabulary or for single term.
 *
 * Method goes through following scenario:
 * 1. gets all entity types info;
 * 2. gets all bundles for each entity type;
 * 3. gets all fields info for each bundle;
 * 4. gets all entities ids with taxonomy field(s) for certain entity type &
 * bundle;
 * 5. loads entity ids been got;
 * 6. gets all values of taxonomy field(s) for each entity;
 * 7. saves values for every field.
 *
 * @param object|null $vocabulary
 * @param object|null $term
 *
 * @return array $references
 */
function _term_reference_weight_get_vocabulary_terms_references($vocabulary = NULL, $term = NULL) {
  if ((empty($vocabulary) && empty($term)) || (!empty($vocabulary) && !empty($term))) {
    throw new BadFunctionCallException('You should provide either $vocabulary or $term.');
  }
  $references = array();

  // Filter terms by vocabulary.
  // Or pass everything - we'll references filter later by term criteria.
  $voc = isset($vocabulary) ? "'" . $vocabulary->machine_name . "'" : 'FALSE';
  $checking_callback = create_function('$item',
    'if (' . $voc . ') {

      return isset($item[\'vocabulary\']) && ' . $voc . ' == $item[\'vocabulary\'];
    }
    else {

      return TRUE;
    }'
  );

  // Sort term references weights ascending.
  $sort_callback = create_function('$a, $b',
    'if ($a[\'weight\'] != $b[\'weight\']) {

      return $a[\'weight\'] - $b[\'weight\'];
    }

    return $a[\'entity\'][\'id\'] - $b[\'entity\'][\'id\'];'
  );

  $info = entity_get_info();

  foreach (array_keys($info) as $entity_type) {

    if ($info[$entity_type]['fieldable'] === TRUE) {
      foreach (array_keys($info[$entity_type]['bundles']) as $bundle_name) {

        // Retrieves information about field instances across separate bundle.
        foreach (field_info_instances($entity_type, $bundle_name) as $field_instance) {

          if ($field_instance['deleted']) {
            continue;
          }

          // Returns data about an individual field.
          $field_info = field_info_field($field_instance['field_name']);

          // Check that field type is "term reference" and field vocabulary is
          // the same as selected on vocabulary edit page.
          if ('taxonomy_term_reference' == $field_info['type']
            && (FALSE !== current(array_filter($field_info['settings']['allowed_values'], $checking_callback)))) {

            $entities_with_taxonomy_field = term_references_weight_get_entities_with_taxonomy_field(
              $entity_type,
              $bundle_name,
              $field_instance,
              $term
            );

            if (!empty($entities_with_taxonomy_field)) {
              $entities_with_taxonomy_field = reset($entities_with_taxonomy_field);
            }
            else {
              continue;
            }

            // Load entities from the database, not require database access
            // if loaded again during the same page request.
            if ($entities = entity_load($entity_type, array_keys($entities_with_taxonomy_field))) {
              foreach ($entities as $entity) {

                if ($field_values = field_get_items($entity_type, $entity, $field_instance['field_name'])) {
                  foreach ($field_values as $field_value) {
                    // Check if term_id exists.
                    // Appears that ids from field table can be not deleted.
                    if ($field_value_term = taxonomy_term_load($field_value['tid'])) {
                      // Check if this is either vocabulary criteria
                      // (term is not defined) or term criteria (and tid is suitable).
                      if (!$term || $term->tid == $field_value['tid']) {

                        // Collect references according to entity type for output by entity type.
                        $references[$entity_type][] = term_reference_weight_get_entities_references(
                          $entity_type,
                          $entity,
                          $field_instance,
                          $field_value_term
                        );

                        // Sort items.
                        uasort($references[$entity_type], $sort_callback);

                      } // term id validation
                    } // term load & existence validation
                  } // field_value loop
                } // field_values validation
              } // end of entities loop
            } // entities load validation
          } // field & vocabulary validation
        } // end of field instances loop
      } // end of bundles loop
    } // fieldable entity_type validation
  } // end of entity types loop

  // Excludes non-unique reference items.
  $references = unique_reference_items($references);

  return $references;
}

/**
 * Gets entities ids attached to every bundle taxonomy term field.
 *
 * According to official documentation: bundle is not supported in comment entity types.
 * http://api.drupal.org/api/drupal/includes--entity.inc/function/EntityFieldQuery%3A%3AentityCondition/7
 * In practice, if bundle name equals entity type name field condition behavior
 * unpredictable.
 *
 * @param string $entity_type
 * @param string $bundle_name
 * @param array $field_instance
 * @param object|null $term
 *
 * @return array|mixed $entities_with_taxonomy_field
 */
function term_references_weight_get_entities_with_taxonomy_field($entity_type,
                                                                 $bundle_name,
                                                                 $field_instance,
                                                                 $term = NULL) {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', $entity_type);
  if (isset($entity_type['bundle_keys'])) {
    $query
      ->entityCondition('bundle', $bundle_name);
  }
  // Vocabulary criteria.
  if (!$term) {
    $query
      ->fieldCondition($field_instance['field_name'], 'tid', NULL, 'IS NOT');
  }
  $entities_with_taxonomy_field = $query->execute();

  return $entities_with_taxonomy_field;
}

/**
 * Makes final preparations and stores entities information.
 *
 * @param string $entity_type
 * @param object $entity
 * @param array $field_instance
 * @param object $field_value_term
 *
 * @return array $reference_items
 *
 * @throws \EntityMalformedException
 */
function term_reference_weight_get_entities_references($entity_type,
                                                       $entity,
                                                       $field_instance,
                                                       $field_value_term) {
  list($id, , $bundle_name) = entity_extract_ids($entity_type, $entity);
  $entity_uri = entity_uri($entity_type, $entity);
  $entity_label = entity_label($entity_type, $entity);
  $weight = _term_reference_weight_get_reference_weight(
    $bundle_name,
    $entity_type,
    $id,
    $field_instance['field_name'],
    $field_value_term->tid
  );
  // Collects reference data.
  $reference_items = array(
    'bundle_name' => $bundle_name,
    'entity' => array(
      'type' => $entity_type,
      'id' => $id,
      'label' => $entity_label,
      'path' => isset($entity_uri) ? $entity_uri['path'] : '',
    ),
    'term' => array(
      'tid' => $field_value_term->tid,
      'name' => $field_value_term->name,
    ),
    'field_instance' => $field_instance,
    'weight' => $weight,
  );

  return $reference_items;
}

/**
 * Uniques reference items.
 *
 * Non-unique term references weights items appears as a result of following
 * circumstances:
 * 1. EntityFieldQuery() can't separate, in this case, term ids of a entity type
 * (such as comment) by bundle name;
 * http://api.drupal.org/api/drupal/includes--entity.inc/function/EntityFieldQuery%3A%3AentityCondition/7
 * 2. _term_reference_weight_get_vocabulary_terms_references() must iterate
 * every bundle name of a given entity type (comment, too).
 *
 * Normally, if an entity type has no real bundle, bundle name is the same as an
 * entity type name. But, in practice, comment entity type may have few bundle
 * names. As a result EntityFieldQuery() called as many times as number of
 * bundle names and every time returns the same query result.
 *
 * @param array $references
 *
 * @return array
 */
function unique_reference_items($references) {
  $temp_array = array();

  foreach ($references as $entity_type => $reference_items) {
    $i = 0;
    $key_bundle_name = array();
    $key_entity_id = array();

    foreach ($reference_items as $reference_item) {
      if (!in_array($reference_item['bundle_name'], $key_bundle_name) ||
          !in_array($reference_item['entity']['id'], $key_entity_id)) {

        $key_bundle_name[$i] = $reference_item['bundle_name'];
        $key_entity_id[$i] = $reference_item['entity']['id'];
        $temp_array[$entity_type][$i] = $reference_item;
      }
      $i++;
    }
  }

  return $temp_array;
}

/**
 * Retrieves weight for reference identified by field name, node nid and term
 * tid.
 *
 * @param string $bundle_name
 * @param string $entity_type
 * @param int $entity_id
 * @param string $field_machine_name
 * @param int $term_id
 *
 * @return int
 */
function _term_reference_weight_get_reference_weight($bundle_name,
                                                     $entity_type,
                                                     $entity_id,
                                                     $field_machine_name,
                                                     $term_id) {

  return (int) db_select('term_references_weights', 'trw')
    ->fields('trw', array('weight'))
    ->condition('field_machine_name', $field_machine_name)
    ->condition('term_tid', $term_id)
    ->condition('entity_type', $entity_type)
    ->condition('bundle_name', $bundle_name)
    ->condition('entity_id', $entity_id)
    ->execute()
    ->fetchField();
}

/**
 * Saves weight for a reference.
 *
 * @param array $general_hidden
 * @param array $reference_weight
 *
 * @throws \Exception
 * @throws \InvalidMergeQueryException
 */
function _term_reference_weight_set_reference_weight($general_hidden, $reference_weight) {
  list($field_machine_name, $term_id, $bundle_name, $entity_type, $entity_id) = $general_hidden;
  db_merge('term_references_weights')
    ->key(array(
      'field_machine_name' => $field_machine_name,
      'term_tid' => $term_id,
      'entity_type' => $entity_type,
      'bundle_name' => $bundle_name,
      'entity_id' => $entity_id,
    ))
    ->fields(array(
      'weight' => $reference_weight,
    ))
    ->execute();
}

/**
 * General theme.
 *
 * @param array $variables
 *
 * @return string
 *   An HTML string representing the themed output.
 *
 * @throws \Exception
 */
function theme_term_reference_weight_theme_references_weights_control($variables) {
  $element = $variables['element'];
  $rows = array();
  $path_args = arg();

  foreach (element_children($element) as $key) {
    if (isset($element[$key]['general_hidden'])) {
      $row = array();
      $row[] = drupal_render($element[$key]['general_hidden'])
        . drupal_render($element[$key]['entity_name']);
      $row[] = drupal_render($element[$key]['field_human_title']);

      // Exclude term name from table output.
      if ($path_args[3] != 'edit') {
        $row[] = drupal_render($element[$key]['term_name']);
      }
      $element[$key]['weight']['#attributes']['class'] = array('term-ref-weight');
      $row[] = drupal_render($element[$key]['weight']);
      $rows[] = array('data' => $row, 'class' => array('draggable'));
    }
  }

  $header = array(
    t('Entity title'),
    t('Taxonomy field label'),
  );
  if ($path_args[3] != 'edit') {
    $header = array_merge($header, array(
      t('Term name'),
      t('Weight'),
    ));
  }
  else {
    $header = array_merge($header, array(
      t('Weight'),
    ));
  }

  drupal_add_tabledrag('term-ref-table', 'order', 'sibling', 'term-ref-weight');

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('This vocabulary term is not attached to any entity.'),
    'sticky' => TRUE,
    'attributes' => array(
      'id' => 'term-ref-table',
    ),
  ));
}
