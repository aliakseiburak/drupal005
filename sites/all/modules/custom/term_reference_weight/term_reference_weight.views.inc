<?php
/**
 * @file
 * Definition of term_references_weight_views_join_custom.
 */

/**
 * Implements hook_views_data().
 */
function term_reference_weight_views_data() {
  $data = array();

  $data['term_references_weights'] = array();
  $data['term_references_weights']['table']['group'] = t('Taxonomy term');
  $data['term_references_weights']['table']['join'] = array(
    'taxonomy_term_data' => array(
      'handler' => 'term_references_weight_views_join_custom',
      'left_field' => 'tid',
      'field' => 'term_tid',
    ),
  );

  $data['term_references_weights']['weight'] = array(
    'title' => t('Term attached to entity reference weight (trw)'),
    'help' => t('Term attached to entity reference weight (trw).'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Custom views join handler.
 */
class term_references_weight_views_join_custom extends views_join {

  /**
   * Identifies join rules for entity_type table term_reference_weight table.
   *
   * @parent method
   *   Builds the SQL for the join this object represents.
   */
  public function build_join($select_query, $table, $view_query) {

    $entity_types_list = entity_get_info();
    foreach ($entity_types_list as $entity_type) {

      // Finds exact entity_type and evoke bundle names from its info data.
      if (isset($entity_type['base table']) && $entity_type['base table'] == $view_query->base_table) {

        foreach (array_keys($entity_type['bundles']) as $bundle) {
          // Condition for bundle name select.
          $this->extra = sprintf('%s.bundle_name = %s.%s', $table['alias'], $view_query->base_table, $bundle);
        }
        // Condition for entity id select.
        $this->extra = sprintf('%s.entity_id = %s.%s', $table['alias'], $view_query->base_table, $view_query->base_field);
        parent::build_join($select_query, $table, $view_query);
      }
    }
  }

}
